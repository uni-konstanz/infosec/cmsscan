#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# cmsscan.sh
# This is a wrapper for multiple cms vulnerability scanners, to provide an easy
# to use and common interface, to quickly scan many domains.
# The script can take a single domain or a list of domains as input.
# It first uses `droopescan` for an initial assessment, and then uses specific
# tools to scan individual cms systems.
###

# set -e -o pipefail # don't set to avoid exiting on droopescan or wpscan errors
shopt -s failglob

# check for existing commands
command -v droopescan >/dev/null 2>&1 || { echo >&2 "ERROR: droopescan required."; exit 1; }
command -v wpscan >/dev/null 2>&1 || { echo >&2 "ERROR: wpscan required."; exit 1; }

# set and create output directory
OUTDIR="${HOME}/cmsscan_reports"

# check for domain list
usage() {
	echo "usage: $(basename "$0") <domain_list_file>"
	echo ""
	echo "Scan a list of domains for existing CMS and try to identify the specific CMS."
	echo "If a wordpress instance is found, automatically scans with wpscan."
	echo ""
	echo "Writes result reports as txt files to $OUTDIR."
	exit 0;
}

# check parameters
[[ -z "$1" ]] && { usage; exit 1; }
[[ ! -f "$1" ]] && { echo "ERROR: file $1 not found!"; usage; exit 1 }
DOMAINS="$1"

# set vars for convenience
PREFIX="$(date -I)"
REPORT="report.txt"
DROOPE="droopescan_${REPORT}"
WP="wpscan_${REPORT}"

mkdir -p "${OUTDIR}"

# first run droopescan to identify cms
while read -r line; do
	echo "droopescan scan -u $line";
	droopescan scan -u "$line" | tee "${OUTDIR}/${PREFIX}_${line}_${DROOPE}";
done<"${DOMAINS}"

# rescan wordpress instances with wpscan
for i in $(grep -i "identified as wordpress" ${OUTDIR}/*${REPORT} | cut -d ":" -f 1); do
	echo "wpscan -e --follow-redirection --batch --url ${i%DROOPE}"
	wpscan -e --follow-redirection --batch --url "${i%DROOPE}" | tee "${OUTDIR}/${i%DROOPE}${WP}";
done
